# Commerce Stripe Klarna
INTRODUCTION
------------
This module is a standalone integration of Klarna as a Stripe payment method

REQUIREMENTS
------------
    - drupal/core: ^8.8 || ^9
    - drupal/commerce : ^2.19
    - stripe/stripe/php: ^7.25

INSTALLATION
------------
Add this module via composer
```
composer require 'drupal/commerce_stripe_klarna:^1.0'
```

CONFIGURATION
------------
 - Go to admin/commerce/config/payment-gateways
 - Click on add a new payment gateway.
 - Find Commerce Klarna Stripe
 - Add your Stripe credentials

 - For US implementation the Stripe account needs to be in the US and
   currency needs to be USD
 - Other countries supported are: AT, BE, DE, DK, ES, FI, GB, IE, IT,
   NL, NO, SE, FR, EE, GR, LV, LT, SK, SI.
 - Other currencies supported are: EUR, GBP, DKK, SEK, or NOK

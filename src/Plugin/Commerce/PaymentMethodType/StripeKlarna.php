<?php

namespace Drupal\commerce_stripe_klarna\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Klarna payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "stripe_klarna",
 *   label = @Translation("Stripe Klarna"),
 *   create_label = @Translation("Stripe Klarna"),
 * )
 */
class StripeKlarna extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->pluginDefinition['label'];
  }

}

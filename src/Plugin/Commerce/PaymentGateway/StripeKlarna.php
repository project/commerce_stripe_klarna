<?php

namespace Drupal\commerce_stripe_klarna\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Language\LanguageInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe_klarna\Event\CommerceStripeKlarnaEvent;
use Drupal\commerce_stripe_klarna\Event\CommerceStripeKlarnaEvents;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Stripe\Balance;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod as StripePaymentMethod;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\StripeObject;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_stripe_klarna",
 *   label = "Stripe Klarna ",
 *   payment_type = "payment_default",
 *   display_label = "Stripe Klarna",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_stripe_klarna\PluginForm\StripeKlarna\PaymentOffsiteForm",
 *   },
 * )
 */
class StripeKlarna extends OffsitePaymentGatewayBase implements SupportsRefundsInterface, SupportsVoidsInterface {

  /**
   * Payment Storage.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentStorage
   */
  protected $paymentStorage;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * UUID core service.
   *
   * @var \\Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerInterface $logger, UuidInterface $uuid, EventDispatcherInterface $event_dispatcher, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    if ($this->configuration['secret_key']) {
      Stripe::setApiKey($this->configuration['secret_key']);
      $this->logger = $logger;
    }
    try {
      $this->paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    } catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->critical($e->getMessage());
    }
    $this->uuid = $uuid;
    $this->eventDispatcher = $event_dispatcher;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')->get('commerce_stripe_klarna'),
      $container->get('uuid'),
      $container->get('event_dispatcher'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'publishable_key' => '',
        'secret_key' => '',
        'logo' => 0,
        'display_title_override' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['publishable_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable Key'),
      '#default_value' => $this->configuration['publishable_key'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['logo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show logo only'),
      '#default_value' => $this->configuration['logo'],
    ];

    $form['display_title_override'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Override display title'),
      '#description' => $this->t('Used for overriding display title. You may use HTML here if you want'),
      '#default_value' => $this->configuration['display_title_override'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      // Validate the secret key.
      $expected_livemode = $values['mode'] == 'live';
      if (!empty($values['secret_key'])) {
        try {
          Stripe::setApiKey($values['secret_key']);
          // Make sure we use the right mode for the secret keys.
          if (
            Balance::retrieve()
              ->offsetGet('livemode') != $expected_livemode
          ) {
            $form_state->setError($form['secret_key'], $this->t('The provided secret key is not for the selected mode (@mode).', ['@mode' => $values['mode']]));
          }
        } catch (ApiErrorException $e) {
          $form_state->setError($form['secret_key'], $this->t('Invalid secret key.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['publishable_key'] = $values['publishable_key'];
      $this->configuration['logo'] = $values['logo'];
      $this->configuration['display_title_override'] = $values['display_title_override'];
    }
  }

  /**
   * Create Authorisation Request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment.
   * @param string $returnUrl
   *   Return url.
   *
   * @return string
   *   Redirect Url.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createRequest(PaymentInterface $payment, $returnUrl) {
    if ($payment->getState()->getValue()['value'] != 'new') {
      throw new \InvalidArgumentException('The provided payment is in an invalid state.');
    }
    $url = FALSE;
    $payment->getAmount();

    $order = $payment->getOrder();

    $intent_id = $order->getData('payment_intent');

    try {

      $intent = $intent_id ? PaymentIntent::retrieve($intent_id) : FALSE;

      $allowed_statuses = [
        PaymentIntent::STATUS_REQUIRES_ACTION,
        PaymentIntent::STATUS_REQUIRES_PAYMENT_METHOD,
        PaymentIntent::STATUS_REQUIRES_CONFIRMATION,
      ];

      if (
        $intent instanceof PaymentIntent && in_array($intent->status, $allowed_statuses) &&
        in_array('klarna', $intent->payment_method_types)
      ) {
        if ($intent->next_action instanceof StripeObject) {
          $url = $intent->next_action->offsetGet('redirect_to_url')
            ->offsetGet('url');
        } else {
          $intent = $intent->confirm([
            'return_url' => $returnUrl,
          ]);

          if ($intent->next_action instanceof StripeObject) {
            $url = $intent->next_action->offsetGet('redirect_to_url')
              ->offsetGet('url');
          }
        }
      } else {
        $orderBillingProfile = $order->getBillingProfile();
        $billingAddress = $orderBillingProfile->get('address')->first()->toArray();
        $payment_method = StripePaymentMethod::create([
          'type' => 'klarna',
          'billing_details' => [
            'address' => [
              'city' => $billingAddress['locality'],
              'country' => $billingAddress['country_code'],
              'line1' => $billingAddress['address_line1'],
              'line2' => $billingAddress['address_line2'],
              'postal_code' => $billingAddress['postal_code'],
              'state' => $billingAddress['administrative_area'],
            ],
            "email" => $order->getEmail(),
            "name" => implode(' ', [$billingAddress['given_name'], $billingAddress['family_name']]),
            "phone" => isset($billingAddress['field_phone']) ? $billingAddress['field_phone'] : '',
          ],
        ]);

        $lang = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
        $orderCurrencyCode = $order->getTotalPrice()->getCurrencyCode();
        $acceptedCurrencies = [
          'AUD',
          'CAD',
          'CHF',
          'CZK',
          'DKK',
          'EUR',
          'GBP',
          'NOK',
          'NZD',
          'PLN',
          'SEK',
          'USD',
        ];

        if (!in_array($orderCurrencyCode, $acceptedCurrencies)) {
          throw new \InvalidArgumentException('The provided currency is not supported.');
        }

        $intent = PaymentIntent::create([
          'amount' => $this->toMinorUnits($order->getTotalPrice()),
          'currency' => $orderCurrencyCode,
          'payment_method_types' => ['klarna'],
          'payment_method_options' => ['klarna' => ['preferred_locale' => $this->getKlarnaPreferredLanguage($billingAddress['country_code'], $lang)]],
          'payment_method' => $payment_method,
          'description' => $this->t('Klarna: Order @order', [
            '@order' => $order->id(),
          ]),
          'metadata' => [
            'order_id' => $order->id(),
            'store_id' => $order->getStoreId(),
            'store_name' => $order->getStore()->label(),
            'email' => $order->getEmail(),
          ],
        ]);

        $order->setData('payment_intent', $intent->id)->save();
        $order->setData('payment_intent_client_secret', $intent->client_secret)->save();

        $intent = $intent->confirm([
          'return_url' => $returnUrl,
        ]);

        if ($intent->next_action instanceof StripeObject) {
          $url = $intent->next_action->offsetGet('redirect_to_url')
            ->offsetGet('url');
        }

        // Update the local payment entity.
        $payment->setState('authorization');
        $payment->setRemoteId($intent->id);
        $payment->save();
      }
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
    }

    return $url;
  }

  /**
   * Get the Klarna preferred language for the interface
   *
   * @param string $billingCountry
   * @param string $language
   *
   * @return string
   */
  private function getKlarnaPreferredLanguage(string $billingCountry, string $language): string {
    $billingCountry = strtoupper($billingCountry);
    $default = 'en-US';
    $imploded = implode('-', [$language, $billingCountry]);
    $altImplodedByBilling = implode('-', ['en', $billingCountry]);
    switch ($language) {
      case 'en':
        //example case en-GB
        if (in_array($imploded, $this->acceptedPreferredLanguages())) {
          return $imploded;
        }
        return $default;

      default: //all other languages

        //example case de-DE
        if (in_array($imploded, $this->acceptedPreferredLanguages())) {
          return $imploded;
        }

        //example case de-AT
        if (in_array($altImplodedByBilling, $this->acceptedPreferredLanguages())) {
          return $altImplodedByBilling;
        }

        foreach ($this->allKlarnaLanguagesWithoutEn() as $altLang) {
          $altImplodedByLang = implode('-', [$altLang, $billingCountry]);
          //example case es-US
          if (in_array($altImplodedByLang, $this->acceptedPreferredLanguages())) {
            return $altImplodedByLang;
          }
        }

        //else return defualt en-US
        return $default;
    }
  }

  /**
   * Get all Klarna accepted languages without English.
   * Reason the  english is skipped is because We are trying to find an alternative language for the user interface
   *
   * @return array
   */
  private function allKlarnaLanguagesWithoutEn(): array {
    return [
      'cs',
      'de',
      'el',
      'da',
      'es',
      'fi',
      'fr',
      'it',
      'nb',
      'nl',
      'pl',
      'pt',
      'sv',
    ];
  }

  /**
   * Get all Klarna accepted languages.
   *
   * @return array
   */
  private function acceptedPreferredLanguages(): array {
    return [
      'cs-CZ',
      'de-CH',
      'de-DE',
      'da-DK',
      'en-AU',
      'en-AT',
      'en-CA',
      'en-DK',
      'en-DE',
      'en-FI',
      'en-NL',
      'en-NZ',
      'en-NO',
      'en-SE',
      'en-CH',
      'en-GB',
      'en-US',
      'en-BE',
      'en-ES',
      'en-IT',
      'en-FR',
      'en-IE',
      'en-PL',
      'en-PT',
      'en-CZ',
      'en-GR',
      'el-GR',
      'es-US',
      'es-ES',
      'fi-FI',
      'fr-BE',
      'fr-CH',
      'fr-FR',
      'fr-CA',
      'it-CH',
      'it-IT',
      'nb-NO',
      'nl-NL',
      'nl-BE',
      'pl-PL',
      'pt-PT',
      'sv-SE',
      'sv-FI',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_intent = $request->get('payment_intent');
    $payment_intent_client_secret = $request->get('payment_intent_client_secret');
    $parameters = [];
    $payment = $this->findPayment($payment_intent, $payment_intent_client_secret);

    // Payment with matching payment intent and client secret not found.
    if (is_null($payment)) {
      throw new InvalidRequestException("'Invalid payment specified.");
    }

    $order = $payment->getOrder();
    if ($order->isPaid()) {
      throw new InvalidRequestException('Order for this payment is already paid in full');
    }

    if ($payment->getState()->getValue()['value'] !== 'completed') {
      /** @var \Stripe\StripeAlipay $gateway */
      try {
        $intent = PaymentIntent::retrieve($payment_intent);
      } catch (ApiErrorException $e) {
        throw new InvalidRequestException('Unable to payment intent');
      }

      switch ($intent->status) {
        case PaymentIntent::STATUS_SUCCEEDED:
          $this->intentSucceeded($intent);
          $payment = $this->paymentStorage
            ->load($payment->id());
          break;

        case PaymentIntent::STATUS_REQUIRES_PAYMENT_METHOD:
        case PaymentIntent::STATUS_CANCELED:
        case PaymentIntent::STATUS_REQUIRES_ACTION:
          // Void transaction.
          $payment = $this->intentPaymentFailed($intent);
          $this->messenger()->addStatus('Payment failed.');
          break;
      }
    }

    // Now we know any outstanding actions have been resolved.
    // If the payment has been completed in webhooks there's nothing to do.
    switch ($payment->getState()->getValue()['value']) {
      case 'completed':
        $parameters = [
          'payment_intent' => $request->get('payment_intent'),
        ];
        $route = 'commerce_checkout.form';
        break;

      default:
        $route = 'commerce_checkout.form';
        break;
    }

    $url = Url::fromRoute($route, [
      'commerce_order' => $order->id(),
      'step' => 'checkout',
    ], [
      'query' => $parameters,
      'absolute' => TRUE,
    ])->toString();
    $response = new RedirectResponse($url);
    $response->send();
    return $response;
  }

  /**
   * Finds the payment by remote_id and client_secret combination.
   *
   * @param string $remote_id
   *   Remote id.
   * @param string|bool $client_secret
   *   If not empty checks if client secret matches the one on payment intent.
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Either the found payment entity or null if nothing could be found.
   */
  public function findPayment($remote_id, $client_secret = FALSE) {
    /** @var \Drupal\commerce_payment\Entity\Payment[] $payments */

    $payments = $this->paymentStorage
      ->loadByProperties([
        'remote_id' => $remote_id,
      ]);
    if (empty($payments)) {
      return NULL;
    }
    $payment = reset($payments);
    if ($payment instanceof Payment && $client_secret) {
      $order = $payment->getOrder();
      if ($order instanceof Order && $client_secret != $order->getData('payment_intent_client_secret')) {
        // Payment doesn't match client secret.
        $payment = NULL;
      }
    }

    return $payment;
  }

  /**
   * Handles the payment_intent.succeeded of a Stripe webhook.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentIntent $intent
   *   The data Stripe provides for this event.
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function intentSucceeded(PaymentIntent $intent) {
    $payment = $this->findPayment($intent->id, $intent->client_secret);
    if ($payment instanceof Payment) {
      $workflow = $payment->getState()->getWorkflow();
      $transition = $workflow->getTransition('capture');
      $payment->getState()->applyTransition($transition);
      $request_time = $this->time->getRequestTime();
      $payment->setCompletedTime($request_time);
      $payment->save();
      $event = new CommerceStripeKlarnaEvent($payment);
      $this->eventDispatcher->dispatch($event, CommerceStripeKlarnaEvents::COMMERCE_STRIPE_KLARNA_PAYMENT_SUCCEEDED);
    }
    return $payment;
  }

  /**
   * Handles the payment_intent.payment_failed event of a Stripe webhook.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentIntent $intent
   *   The data Stripe provides for this event.
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function intentPaymentFailed(PaymentIntent $intent) {
    $payment = $this->findPayment($intent->id, $intent->client_secret);
    if ($payment instanceof Payment) {
      $workflow = $payment->getState()->getWorkflow();
      $transition = $workflow->getTransition('void');
      $payment->getState()->applyTransition($transition);
      $payment->save();
      $event = new CommerceStripeKlarnaEvent($payment);
      $this->eventDispatcher->dispatch($event, CommerceStripeKlarnaEvents::COMMERCE_STRIPE_KLARNA_PAYMENT_FAILED);
    }
    return $payment;
  }

  /**
   * Handles the decoupled checkout.
   *
   * This is an alternative solution to create a payment,
   * but the redirection is handled before
   * within your local decoupled controller/service.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   */
  public function onDecoupledReturn(OrderInterface $order, Request $request) {
    $payment_intent = $request->get('payment_intent');
    $payment_intent_client_secret = $request->get('payment_intent_client_secret');

    $payment = $this->findPayment($payment_intent, $payment_intent_client_secret);

    /** @var \Stripe\StripeKlarna $gateway */
    try {
      $paymentIntent = PaymentIntent::retrieve($payment_intent);
    } catch (ApiErrorException $e) {
      throw new InvalidRequestException('Unable to retrieve payment intent');
    }

    switch ($paymentIntent->status) {
      case PaymentIntent::STATUS_SUCCEEDED:
        /*
         * if payment is missing create a new payment.
         * State is set to completed, since the payment is processed
         * on the frontend side.
         */
        if (!$payment) {
          $current_user = $order->getCustomerId();
          $paymentGateway = $order->get('payment_gateway')->entity;
          $remoteId = $request->get('payment_intent');
          if ($paymentIntent->status != PaymentIntent::STATUS_SUCCEEDED) {
            $remoteId = '';
          }

          if (empty($remoteId) && $request->query->has('payment_intent')) {
            $this->logger
              ->error($this->t('Payment failed at the payment server @order_id. @message', [
                '@order_id' => $order->id(),
              ]));
          }

          $currentTime = $this->time->getRequestTime();

          $paymentMethod = $order->get('payment_method')->getValue();
          if (empty($paymentMethod)) {
            /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
            $paymentMethod = PaymentMethod::create([
              'type' => 'stripe_klarna',
              'payment_gateway' => $paymentGateway->id(),
              'uid' => $order->getCustomerId(),
              'remote_id' => $paymentIntent->payment_method,
            ]);
            $paymentMethod->setReusable(FALSE);
            $paymentMethod->setBillingProfile($order->getBillingProfile());
            $paymentMethod->save();
            $order->set('payment_method', $paymentMethod);
            $order->save();
          }

          $payment = Payment::create([
            'state' => 'completed',
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $paymentGateway->id(),
            'payment_method' => $paymentMethod->id(),
            'order_id' => $order->id(),
            'remote_id' => $remoteId,
            'remote_state' => $paymentIntent->status,
            'payment_gateway_mode' => $paymentGateway->getPlugin()->getMode(),
            'expires' => 0,
            'uid' => $current_user,
            'completed' => $currentTime,
            'authorized' => $currentTime,
          ]);

          $payment->save();

          $order->setData('payment_intent_client_secret', $request->get('payment_intent_client_secret'))
            ->save();
        }
        break;

      case PaymentIntent::STATUS_REQUIRES_PAYMENT_METHOD:
      case PaymentIntent::STATUS_CANCELED:
      case PaymentIntent::STATUS_REQUIRES_ACTION:
        // Void transaction.
        $payment = $this->intentPaymentFailed($paymentIntent);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $intent = PaymentIntent::retrieve($payment->getRemoteId());
      $minor_units_amount = $this->toMinorUnits($amount);
      $data = [
        'charge' => reset($intent->charges->data),
        'amount' => $minor_units_amount,
      ];
      // Refund and support for Idempotent Requests.
      // https://stripe.com/docs/api/idempotent_requests
      Refund::create($data, ['idempotency_key' => $this->uuid->generate()]);
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    } else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Void Stripe payment - release uncaptured payment.
    try {
      $intent = PaymentIntent::retrieve($payment->getRemoteId());
      if ($intent instanceof PaymentIntent) {
        $statuses_to_void = [
          PaymentIntent::STATUS_REQUIRES_CONFIRMATION,
          PaymentIntent::STATUS_REQUIRES_ACTION,
          PaymentIntent::STATUS_REQUIRES_CAPTURE,
          PaymentIntent::STATUS_REQUIRES_PAYMENT_METHOD,
        ];
        if (!in_array($intent->status, $statuses_to_void)) {
          throw new PaymentGatewayException('The PaymentIntent cannot be voided because its not in allowed status.');
        }
        $intent->cancel();
      } else {
        $message = $this->t('Payment intent could not be retrieved from Stripe. Please check data on Stripe.');
        $this->logger->warning($message);
        throw new PaymentGatewayException($message);
      }
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
      throw new PaymentGatewayException('Void failure. Please check Stripe Logs for more info.');
    }

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel() {
    if ($this->configuration['display_title_override']) {
      return Markup::create($this->configuration['display_title_override'])
        ->__toString();
    }
    if ($this->configuration['logo']) {
      return Markup::create('<img alt="Stripe Klarna" src="https://docs.klarna.com/assets/media/e8921e82-4aa9-4126-9f5b-529d04c106ad/Klarna+marketing+badge+%28pink+rgb.svg%29.svg">')
        ->__toString();
    }

    return parent::getDisplayLabel();
  }
}

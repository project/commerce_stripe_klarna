<?php

namespace Drupal\commerce_stripe_klarna\Event;

/**
 * Defines events for the Commerce Stripe Alipay module.
 */
final class CommerceStripeKlarnaEvents {
  /**
   * Name of the event fired when payment succeeded.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_klarna\Event\CommerceStripeKlarnaEvent
   */
  const COMMERCE_STRIPE_KLARNA_PAYMENT_SUCCEEDED = 'commerce_stripe_klarna.payment_succeeded';

  /**
   * Name of the event fired when payment failed.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_klarna\Event\CommerceStripeKlarnaEvent
   */
  const COMMERCE_STRIPE_KLARNA_PAYMENT_FAILED = 'commerce_stripe_klarna.payment_failed';

}
